from sqlalchemy import create_engine, Column, Integer, String, Text, Boolean, ForeignKey
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from settings_and_misc import get_db_string

Base = declarative_base()


class Semester(Base):
    __tablename__ = 'semester'

    date_semester = Column(String, primary_key=True)
    level_semester = Column(String)
    observation = Column(Text)

    def period(self):
        out = f"{self.first_month()} {self.year()} -- {self.last_month()} "
        out += str(int(self.year()) + 1) if self.is_fall() else self.year()
        return out

    def is_fall(self):
        return "A" in self.date_semester

    def type_semester(self):
        return "Fall" if self.is_fall() else "Spring"

    def first_month(self):
        return "September" if self.is_fall() else "February"

    def last_month(self):
        return "January" if self.is_fall() else "July"

    def semester_code(self):
        return self.date_semester[0] + self.date_semester[3:5]

    def year(self):
        return self.date_semester[1:]

    def __repr__(self):
        return f"Semester {self.date_semester} : {self.level_semester}"


class Course(Base):
    __tablename__ = 'course'

    course_code = Column(String, primary_key=True)
    ects = Column(Integer)
    type = Column(String)
    taught_in_spring = Column(Boolean)
    taught_in_fall = Column(Boolean)
    has_final_exam = Column(Boolean)

    def __repr__(self):
        return f"{self.course_code} {self.type} {self.ects} ECTS "


class CourseDescription(Base):
    __tablename__ = "course_description"

    course_code = Column(String, ForeignKey('course.course_code'), primary_key=True)
    lang = Column(String, primary_key=True)

    title = Column(String)
    overview = Column(Text)
    bibliography = Column(Text)
    recommended_level = Column(String)
    assessment_criteria = Column(String)
    success_criteria = Column(String)
    misc = Column(String)

    training_objectives = Column(Text)
    pedagogical_objectives = Column(Text)
    other_objectives = Column(Text)

    curriculum = Column(Text)
    outcomes = Column(Text)

    course = relationship("Course", back_populates="descriptions")

    def __repr__(self):
        return f"Description of {self.course_code} in {self.lang}"


class CourseResult(Base):
    __tablename__ = 'course_result'

    course_code = Column(String, ForeignKey('course.course_code'), primary_key=True)
    date_semester = Column(String, ForeignKey('semester.date_semester'), primary_key=True)
    result = Column(String)

    course = relationship("Course", back_populates="results")
    semester = relationship("Semester", back_populates="results")

    def __repr__(self):
        return f"Course Result of {self.course_code} at semester {self.date_semester}"


# Foreign Keys Management

# CourseDescription.course_code => Course.course_code
Course.descriptions = relationship("CourseDescription", order_by=Course.course_code, back_populates="course")

# CourseResult.course_code => Course.course_code
Course.results = relationship("CourseResult", order_by=Course.course_code, back_populates="course")

# CourseResult.date_semester => Semester.date_semester
Semester.results = relationship("CourseResult", order_by=Semester.date_semester, back_populates="semester")


def create_tables(verbose=False):
    print("Creating database")
    db_string = get_db_string()
    engine = create_engine(db_string, echo=verbose)
    Base.metadata.create_all(engine)


def reset_db(verbose=False):
    print("Resetting database")
    db_string = get_db_string()
    engine = create_engine(db_string, echo=verbose)
    Base.metadata.drop_all(bind=engine)
    Base.metadata.create_all(bind=engine)
    print("Database reset")


if __name__ == "__main__":
    create_tables()
