from os import system
from sqlalchemy import create_engine, func
from sqlalchemy.orm import sessionmaker

from models import Semester, CourseResult, Course, CourseDescription
from settings_and_misc import get_db_string, bl, lang, latex_new_value, beg_doc, end_doc, main_doc, preamble, \
    out_folder, get_student_details


def course_string(course, course_description, result):
    """
    Returns a formatted string in LaTeX of the courses information, results
    and description.

    :param course: a Course object
    :param course_description : a CourseDescription object
    :param result : a CourseResult object
    :return: a string, LaTeX formatted.
    """
    # Defines a cell over 3 consecutive rows
    mr = "\\mr{3}{*}{"

    # Draws a line under specific cells
    cline = lambda start, end: "\\cline{" + str(start) + "-" + str(end) + "}"

    formatted_string = f"%%%% {course.course_code}" + bl
    formatted_string += mr + str(course.course_code) + "} & " + bl
    formatted_string += mr + str(course.type) + "} &" + bl
    formatted_string += "\\textbf{Title} : \\textsc{" + str(course_description.title) + "} &" + bl
    formatted_string += mr + str(course.ects) + "} &" + bl
    formatted_string += mr + str(result.result) + "} \\\\ " + cline(3, 3) + bl + bl

    # We only include those details in they exist
    if course_description.overview != "":
        formatted_string += "&&\\textbf{Overview :} " + str(
            course_description.overview) + "&&\\\\" + bl + cline(3, 3) + bl
    if course_description.curriculum != "":
        formatted_string += "&&\\textbf{Program :} " + str(
            course_description.curriculum) + "&&\\\\" + bl

    formatted_string += cline(1, 5) + bl

    return formatted_string


def semester_string(sem):
    """
    Returns a formatted string in LaTeX for the semester information.
    Courses information and results associated to the semester are not included.

    :param sem: a Semester Object
    :return: a string, LaTeX formatted.
    """
    formatted_string = f"%%%%% Semester {sem.date_semester}" + bl
    multicols = "\\multicolumn{5}{|l|}"

    year = sem.year()
    first_month = sem.first_month()
    last_month = sem.last_month()
    period = sem.period()

    # Start of a period : we add a header
    if "01" in sem.level_semester:
        formatted_string += multicols + "{} \\\\" + bl
        formatted_string += multicols + "{\large\\textsc{"
        if "TC" in sem.level_semester:
            formatted_string += "Two First Preparatory Years (" + first_month + " " + year + \
                                " -- " + last_month + " " + str(int(year) + 2) + ")"
        else:
            formatted_string += "Master of Computer Science \& Engineering (" + first_month + " " + \
                                year + " -- " + last_month + " " + str(int(year) + 3) + ")"
        formatted_string += "}} \\\\" + multicols + "{} \\\\ " + bl + " \\hline" + bl

    formatted_string += multicols + "{} \\\\" + bl
    formatted_string += multicols + "{\\textsc{" + sem.level_semester + " -- " + \
                        sem.semester_code() + " : " + sem.type_semester() + " " + \
                        year + " (" + period + ")}} \\\\" + bl
    formatted_string += multicols + "{} \\\\" + bl
    formatted_string += " \\hline" + bl

    # To add observation about the semester
    # formatted_string += multicols + "{\\textbf{Observation : } " + sem.observation.replace("\n","\\n") + "}\\\\" +
    # bl + " \\hline" + bl + bl
    return formatted_string


def create_tex_and_compile():
    """
    Creates the final .tex file and compiles it using latexmk
    """
    with open(main_doc, "w") as out:

        # Prepending the preamble
        with open(preamble, "r") as preamble_file:
            out.write(preamble_file.read())

        # Inject Student details
        student_details = get_student_details()
        latex_student_details = student_details.latex_dump()

        # Adding total number of credits
        engine = create_engine(get_db_string())
        session_maker = sessionmaker(engine)
        sess = session_maker()

        total_nb_credits = sess.query(func.sum(Course.ects)).join(Course.results).all()[0][0]
        latex_student_details.append(latex_new_value("totalcredits", str(total_nb_credits)))
        out.writelines("%s\n" % info for info in latex_student_details)

        # Appending the header
        with open(beg_doc, "r") as begdoc:
            out.write(begdoc.read())

        # Building the table
        semesters = sess.query(Semester).all()

        for semester in semesters:

            # Injecting the semester info
            out.write(semester_string(semester))

            course_results = sess.query(CourseResult) \
                .filter(CourseResult.date_semester == semester.date_semester)

            for result in course_results:
                course_q = sess.query(Course) \
                    .filter(Course.course_code == result.course_code).all()

                description_q = sess.query(CourseDescription) \
                    .filter(CourseDescription.lang == lang) \
                    .filter(CourseDescription.course_code == result.course_code).all()

                # Soft replacement if nothing is found for the course or its description
                description = CourseDescription() if len(description_q) == 0 else description_q[0]
                course = Course(course_code=description.course_code,
                                ects=None,
                                type=None) if len(course_q) == 0 else course_q[0]

                course_info_latex = course_string(course, description, result).splitlines()

                # Injecting the courses info
                out.writelines("%s\n" % l for l in course_info_latex)

            # To break for a new page on each semester
            # out.write("\\newpage\n")

        # Appending the end of the doc to the file
        with open(end_doc, "r") as enddoc:
            out.write(enddoc.read())

    system(f"latexmk -pdf {main_doc} -output-directory={out_folder}")


if __name__ == "__main__":
    create_tex_and_compile()
